#include <stdio.h>
#include <stdlib.h>

void my_love(){
    printf("%s\n", "I LOVE YOU SO MUCH, TAKE WHATEVER YOU WANT");
    execve("/bin/bash", 0, 0);
}

void print_menu(){
    printf("%s\n", "MAIN MENU");
    printf("%s\n", "1. Change a value on the list");
    printf("%s\n", "2. Print a value on the list");
    printf("%s\n", "3. XOR a value on the list");
    printf("%s\n", "4. Exit");
}

int get_index(){
    int value;
    scanf("%d", &value);
    return value;
}

void change_value(long *list){
    printf("%s\n", "Which item would you like to change?");
    int index = get_index();
    if(!(index > 0 && index < 20)){
        printf("%s\n", "WHY WOULD YOU EVEN ATTEMPT THIS");
        exit(0);
    }
    printf("%s\n", "What number would you like to insert?");
    int value = get_index();
    list[index] = value;
}

void print_value(long *list){
    printf("%s\n", "Which item would you like to print?");
    int index = get_index();
    if(index < 0){
        printf("%s\n", "WHY WOULD YOU EVEN ATTEMPT THIS");
        exit(0);
    }
    printf("%ld\n", list[index]);
}

void xor_value(long *list){
    printf("%s\n", "Which list item would you like to xor?");
    int index = get_index();
    printf("%s\n", "What would you like to xor this item with?");
    long value;
    scanf("%ld", &value);
    printf("DEBUG: ORIGINAL POINTER: %p\n", list[index]);
    list[index] ^= value;
    printf("DEBUG: WRITING %p TO %p\n", list[index], list + index);
}

int main(){
    printf("%s\n\n", "WELCOME TO THE XOR MACHINE. EXPLOIT ME AND EARN MY SWEET LOVE.");
    long list[20];
    int selection;
    memset(list, 0, sizeof(long) * 20);

    while(1){
        print_menu();
        scanf("%d", &selection);
        switch(selection){
            case 1:
                change_value(list);
                break;
            case 2:
                print_value(list);
                break;
            case 3:
                xor_value(list);
                break;
            case 4:
                return;
        }

    }


}
