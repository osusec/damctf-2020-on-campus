# xor_machine

I WANT TO LOVE YOU

Flag: `dam{update_this_with_real_flag_later}`

Hint `n`: hint

## Stuff to fill in
* Makefile
  * Set `CHALLENGE_NAME`
* src/
  * Put your code in
  * If you use more than just `pwn.c` update the Makefile

## CTFd Files/Connection Info

URL or `nc` info

Files to distribute (binary, libc, etc)

## Hosting Information

What version of Ubuntu/libc (tcache is present on 17.10+)?

Any notes about how to host the challenge or special considerations. If the challenge needs multiple containers, what type of network comms b/t them?
