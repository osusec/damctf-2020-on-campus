#ifndef PWN_H
#define PWN_H

#include <limits.h>
#include <stddef.h>

#define SONG_LEN 32
#define SONG_LIMB_BITS (sizeof(unsigned long)*CHAR_BIT)
#define SONG_LEN_BITS (SONG_LEN*SONG_LIMB_BITS)

struct song
{
	size_t len;
	char name[32];
	unsigned long a[SONG_LEN];
	unsigned long c[SONG_LEN];
};

extern struct song song_db[];
extern size_t song_db_len;

extern const char* palette;

void get_input(struct song* song);
int check_song(struct song* song);
void puts_encoded(const char* str);

#endif
