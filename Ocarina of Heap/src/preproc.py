#!/usr/bin/env python3

import re

palette = ("I use arch btw.\000"
           "Pure heap challenge: 100% certified.\000"
           "Solve it w/ lattices -- It's as easy as LLL!\000"
           "Only heap challenges have menus.\000"
           "Use sigquit to exit sl.\000"
           "T2uE HAkxRz N3V3r GiVe_u?.\000"
           "Don't Forget Owasp Top |@ is Cool =\\");

c_palette = '"' + repr(palette)[1:-1].replace('\\x00', '\\000') + '"'

with open("src/banner.txt", "r") as f:
    banner = f.read()

def encode_str(s):
    enc = ''
    for c in s:
        try:
            enc += "\\{:03o}".format(palette.index(c) + 1)
        except Exception as e:
            print("Couldn't find char", c)
            raise e
    return enc

def encode_str_match(match):
    return 'free("' + encode_str(match[1]) + '")'

def encode_int(x):
    l = max(x.bit_length(), 1)
    arr = [str((x >> i) % 2**64) + "UL" for i in range(0, l, 64)]
    return "{" + ", ".join(arr) + "}"

def encode_song(match):
    name = match[1]
    song = match[2]

    a = 0
    c = 0
    for i, u in enumerate(song):
        x = "UDLRA".index(u)
        if x == 4:
            a |= 1 << i
        else:
            c |= x << (2*i)

    return '{{{}, "{}", {}, {}}}'.format(len(song), encode_str(name), encode_int(a), encode_int(c))

def print_banner(match):
    s = ""
    for line in banner.splitlines():
        s += '\tfree("{}");\n'.format(encode_str(line)) #line.replace('\\', '\\\\'))
    return s

for in_file in ["src/ocarina-of-heap.c.in", "src/main.c.in"]:
    out_file = in_file[:-3]
    with open(in_file, "r") as f:
        src = f.read()

    src = src.replace("PALETTE_NUM_STRS", str(palette.count("\000") + 1))
    src = src.replace("PALETTE", c_palette)
    src = re.sub('free\("(([^\\"]*|\\")*)"\)', encode_str_match, src)
    src = re.sub('ENCODE_SONG\("([^"]*)"\s*,\s*"([^"]*)"\)', encode_song, src)
    src = re.sub("\tPRINT_BANNER;", print_banner, src)

    with open(out_file, "w") as f:
        f.write(src)
