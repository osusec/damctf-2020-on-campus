# Ocarina of Heap

Play the ocarina to free the flag from Ganon's grasp.

Hint `1`: Be sure to run strings!

Hint `2`: Use `one_gadget -l 10`

Flag: `dam{M4y_T4e_W4Y_0F_7h3_h32O_l3ad_t0_74e_t2IfL4g}`

## Stuff to fill in
* Makefile
  * Set `CHALLENGE_NAME`
* src/
  * Put your code in
  * If you use more than just `pwn.c` update the Makefile

## CTFd Files/Connection Info

URL or `nc` info

Distribute binary, libc, ld-linux, and src/ocarina-of-heap.{c,h}.

## Hosting Information

What version of Ubuntu/libc (tcache is present on 17.10+)?

Any notes about how to host the challenge or special considerations. If the challenge needs multiple containers, what type of network comms b/t them?
