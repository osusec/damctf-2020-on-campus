#!/usr/bin/env python

import os
import shutil
import marshal
import originalchallenge
from binascii import hexlify


cso = marshal.dumps(originalchallenge.some_func.__code__)

with open("originalchallenge.py", "r") as fp1:
    cc1 = fp1.read()

with open("challenge.py", "w") as fp2: 
    state = None
    for line in cc1.split("\n"):
        if "#!OBF_start" in line:
            state = "obf"
        elif "#!OBF_end" in line:
            state = None

        elif "#!OBF_call some_func" in line:
            o = line.find("#!")
            fp2.write(" "*o+'code = marshal.loads(binascii.unhexlify("%s"))\n' % hexlify(cso).decode())
            fp2.write(" "*o+'some_func = types.FunctionType(code, globals(), "some_func")'+"\n")

        else:
            if state == None:
                fp2.write(line+"\n")
