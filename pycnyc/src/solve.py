#!/usr/bin/env python3

from base64 import b64decode

intercepted = b64decode("lPRxrBJddZrmbHkxLfWGIJ4Sp6GyxA==")

msg = ""
prev = intercepted[0]
for c in intercepted[1:]:
    dec = c ^ (prev**2 % 256)
    prev = dec
    msg += chr(dec)

print(msg)
