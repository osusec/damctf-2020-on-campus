#!/usr/bin/env python

import sys
import time
import marshal
import types
import signal
import hmac 
import hashlib
import binascii
from base64 import b64encode
try:
    from Crypto.Util import number
except ImportError:
    pass


#!OBF_start
def some_func(message):
    if False:  # used by obfuscation
        x = 7/0
        x = 7/0

    iv = 0
    for l in message:
        iv += ord(l)

    iv = iv % 256

    encrypted = [iv]
    prev = iv
    for lt in message:
        encrypted.append(ord(lt) ^ (prev**2 % 256))
        prev = ord(lt)

    return b64encode(bytes(encrypted)).decode()

#!OBF_end


def send_challenge(uinput):

    #!OBF_call some_func

    
    ck = some_func

    return ck(uinput)


def main():
    u_in = input("Enter message to encrypt: ")
    print(send_challenge(u_in))


if __name__ == "__main__":
    main()

