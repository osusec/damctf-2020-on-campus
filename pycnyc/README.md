# Pycnyc

We found this weird file that seems to be doing some sort of encoding.
Can you decode the intercepted flag?


Flag: `dam{DeCoMP1Le_a_SNek}`

Hint `1`: There is some marshaled algorithm. Can you disassemble it?

Hint `2` This might come in handy: https://docs.python.org/3/library/dis.html

## CTFd Files/Connection Info

This is a reverse challenge so no URL.

Just provide `challenge.py` along with `intercepted.txt`.

## Hosting Information

Nothing special, just distribute the files.
