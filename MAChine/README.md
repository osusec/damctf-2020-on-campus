# MAChine

Server allows execution of arbitrary commands, as long as the shell command is provided along with a valid MAC tag.
Can you find a way to forge the MAC and get the flag?

Flag: `dam{My_@Wesome_MAC_1s_n0THing_bUt_sEcURe}`

Hint `1`: xor is pretty cool

## CTFd Files/Connection Info

URL or `nc` info

Files to distribute (binary, libc, etc)

## Hosting Information
The challenge is a python socket server `machine.py`, running on port 1337. I need a way to execute linux commands, thus plain python image will likely not work.
Additionally, `flag.txt` must have permissions set in such a way that the user running the python script can read but not write to it (preferably the entire directory not writeable).
Lastly, I need `cowsay`, and `/bin/bash` installed. If it's an issue let me know and I'll remove them as possible commands (both are for flavor).

**Note**: Python script is currently in `src/` but it should be run from the same directory where `flag.txt` is.
