var arrays = [
  "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
, "1"
, "2"
, "2"
, "3"
, "4"
, "5"
, "6"
, "7"
, "8"
]
var rand = arrays[Math.floor(Math.random() * arrays.length)];



var random_boolean = Math.random() >= 0.5;

if (rand == "1") {
  startRedShade();

}

if (rand == "2") {
  startRedBinary();

}

if (rand == "3") {
  startRedMaze();

}

if (rand == "4") {
  startRedQuad();

}

if (rand == "5") {
  startRedExcl();

}

if (rand == "6") {
  startRedHeartss();

}

if (rand == "7") {
  startRedEvil();

}

if (rand == "8") {
  startRedRandom();

}

function startSki() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
	  
      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];

      var watercolors = ["#4286f4", "#1856ba", "#5381cc", "#095de8", "#5e7396", "#2b4570", "#102851", "#033fa5", "#498dff", "#09234f"]
      var watercolor = watercolors[Math.floor(Math.random() * watercolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
	  
	  var sixtyninetexts = ["6", "9"]
      var sixtyninetext = sixtyninetexts[Math.floor(Math.random() * sixtyninetexts.length)];
	  
	  var samsatexts = ["\u2654", "\u2655"]
      var samsatext = samsatexts[Math.floor(Math.random() * samsatexts.length)];
	
	  var xxxtexts = ["X", "+"]
      var xxxtext = xxxtexts[Math.floor(Math.random() * xxxtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = pinkscale;
      ctx.fillText( xxxtext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startXXX() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
	  
      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];

      var watercolors = ["#4286f4", "#1856ba", "#5381cc", "#095de8", "#5e7396", "#2b4570", "#102851", "#033fa5", "#498dff", "#09234f"]
      var watercolor = watercolors[Math.floor(Math.random() * watercolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
	  
	  var sixtyninetexts = ["6", "9"]
      var sixtyninetext = sixtyninetexts[Math.floor(Math.random() * sixtyninetexts.length)];
	  
	  var samsatexts = ["\u2654"]
      var samsatext = samsatexts[Math.floor(Math.random() * samsatexts.length)];
	
	  var xxxtexts = ["X", "+"]
      var xxxtext = xxxtexts[Math.floor(Math.random() * xxxtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = redscale;
      ctx.fillText( xxxtext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startWater() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
	  
      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];

      var watercolors = ["#4286f4", "#1856ba", "#5381cc", "#095de8", "#5e7396", "#2b4570", "#102851", "#033fa5", "#498dff", "#09234f"]
      var watercolor = watercolors[Math.floor(Math.random() * watercolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
	  
	  var sixtyninetexts = ["6", "9"]
      var sixtyninetext = sixtyninetexts[Math.floor(Math.random() * sixtyninetexts.length)];
	  
	  var samsatexts = ["\u2654"]
      var samsatext = samsatexts[Math.floor(Math.random() * samsatexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = watercolor;
      ctx.fillText( binary, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startSamsa() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
	  
      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
	  
	  var sixtyninetexts = ["6", "9"]
      var sixtyninetext = sixtyninetexts[Math.floor(Math.random() * sixtyninetexts.length)];
	  
	  var samsatexts = ["\u2654"]
      var samsatext = samsatexts[Math.floor(Math.random() * samsatexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = benniecolor;
      ctx.fillText( samsatext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startSixtyNine() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
	  
	  var sixtyninetexts = ["6", "9"]
      var sixtyninetext = sixtyninetexts[Math.floor(Math.random() * sixtyninetexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = pinkscale;
      ctx.fillText( sixtyninetext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startMatrix() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = moneycolor;
      ctx.fillText( binary, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startBool() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];
	  
	  var boolcolors = ["#be1e1e", "#df3434", "#cd4747", "#902727", "#be6a6a", "#ff2929", "#bd0000", "#ff6161", "#990000", "#d84141"]
      var boolcolor = boolcolors[Math.floor(Math.random() * boolcolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
	  
	  var booltexts = ["b", "o", "l",]
      var booltext = booltexts[Math.floor(Math.random() * booltexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = boolcolor;
      ctx.fillText( booltext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startBennie() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];


  //colors


  //texts

      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = benniecolor;
      ctx.fillText( bennietext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startMoney() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];

      var benniecolors = ["#fffa00", "#fffb49", "#fffb72", "#e0da00", "#efea37", "#7fff00", "#93ff28", "#a9ff54", "#79d61d", "#64c107"]
      var benniecolor = benniecolors[Math.floor(Math.random() * benniecolors.length)];

      var moneycolors = ["#0eb746", "#1e9645", "#037c2b", "#275e39", "#47ad68", "#57e585", "#21ed63", "#0fe253", "#1d6634", "#3b704c"]
      var moneycolor = moneycolors[Math.floor(Math.random() * moneycolors.length)];
  //colors


  //texts
      var bennietexts = ["\u2764", "\u2661", "\u2665", "\u273F", "\u611B", "\u611B"]
      var bennietext = bennietexts[Math.floor(Math.random() * bennietexts.length)];
	  
      var moneytexts = ["¢", "$", "€"]
      var moneytext = moneytexts[Math.floor(Math.random() * moneytexts.length)];

      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = moneycolor;
      ctx.fillText( moneytext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startBlossom() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];

      var pinks = ["#ff91f9", "#ff77f7", "#ff6bf6", "#ff60f5", "#ff44f3", "#dd3bd3", "#c42fbb", "#e021d4", "#af13a6", "#e82edd"]
      var pinkscale = pinks[Math.floor(Math.random() * pinks.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];

      var blossomar = ["\u273F", "\u273F"]
      var blossom = blossomar[Math.floor(Math.random() * blossomar.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = pinkscale;
      ctx.fillText( blossom, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedRandom() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad, binary]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
      ctx.fillText( randomtext, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedHeartss() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = redscale;
      ctx.fillText( heart, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedExcl() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
      ctx.fillText( excl, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedQuad() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
      ctx.fillText( quad, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedMaze() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
      ctx.fillText( maze, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedShade() {

                    var w = c.width = window.innerWidth,
                        h = c.height = window.innerHeight,
                        ctx = c.getContext( '2d' ),

                        tick = 0,

                        particles = [],

                        maxRadius = Math.sqrt( w*w/4 + h*h/4 );

                    ctx.font = '12px monospace';

                    var rand = Math.random() * (0.06 - 0.01);
                    var rand2 = Math.random() * (10 - 6);

                    function getRandomColorr() {
                        var letters = '0123456789ABCDEF';
                        var color = '#';
                        for (var i = 0; i < 6; i++ ) {
                            color += letters[Math.floor(Math.random() * 16)];
                        }
                        return color;
                    }



                    function Particle(){

                      this.reset();
                    }
                    Particle.prototype.reset = function(){

                      this.radian = Math.random() * Math.PI * 2;
                      this.radius = 0;
                      this.angSpeed = rand;
                      this.incSpeed = rand2;

                      this.x = this.y = 0;
                    }
                    Particle.prototype.step = function(){

                      var prevX = this.x,
                          prevY = this.y;

                      this.radian += this.angSpeed;
                      this.radius += this.incSpeed;

                      this.x = this.radius * Math.cos( this.radian );
                      this.y = this.radius * Math.sin( this.radian );

                      var dx = this.x - prevX,
                          dy = this.y - prevY,
                          len = Math.sqrt( dx*dx + dy*dy );

                      for( var i = 0; i <= len; i += 10 ){

                        var y = prevY + dy * i / len,
                            x = prevX + dx * i / len;

                        var posX = ( x / 10 |0 ) * 10,
                            posY = ( y / 10 |0 ) * 10;

                    //colors
                        var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
                        var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

                        var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
                        var redscale = redscales[Math.floor(Math.random() * redscales.length)];

                        var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
                        var color420 = colors420[Math.floor(Math.random() * colors420.length)];

                        var july4 = ["#ff0000", "#ffffff", "#0048ff"]
                        var july4th = july4[Math.floor(Math.random() * july4.length)];
                    //colors


                    //texts
                        var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
                        var maze = mazee[Math.floor(Math.random() * mazee.length)];

                        var binaryar = ["0", "1"]
                        var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

                        var devil = ["6", "6", "6"]
                        var evil = devil[Math.floor(Math.random() * devil.length)];

                        var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
                        var quad = quads[Math.floor(Math.random() * quads.length)];

                        var excla = ["!", "?"]
                        var excl = excla[Math.floor(Math.random() * excla.length)];

                        var shades = ["\u2591", "\u2592", "\u2593"]
                        var shade = shades[Math.floor(Math.random() * shades.length)];

                        var hearts = ["\u2764", "\u2661", "\u2665"]
                        var heart = hearts[Math.floor(Math.random() * hearts.length)];

                        var randomtexts = [shade, excl, heart, maze, evil, quad]
                        var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
                    //texts


                    // BG Color
                    		ctx.fillStyle = '#000000';
                    // BG Color

                    		ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

                    // change these      vvv
                        ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
                        ctx.fillText( shade, w/2 + posX, h/2 + posY );
                    // change these    ^^^
                      }

                      if( this.radius >= maxRadius )
                        this.reset();
                    }

                    function anim(){

                      window.requestAnimationFrame( anim );

                      ++tick;

                      ctx.fillStyle = 'rgba(0,0,0,0)';
                      ctx.fillRect( 0, 0, w, h );

                      if( particles.length < 100 && Math.random() < .3 )
                        particles.push( new Particle );

                      particles.map( function( particle ){ particle.step(); } );

                    }
                    ctx.fillStyle = '#00000';
                    ctx.fillRect( 0, 0, w, h );
                    anim();

                    window.addEventListener( 'resize', function(){

                      w = c.width = window.innerWidth;
                      h = c.height = window.innerHeight;
                      ctx.font = '12px monospace';

                    	ctx.fillStyle = '#00000';
                    	ctx.fillRect( 0, 0, w, h );

                      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

                    })
}

function startRedEvil() {
  var w = c.width = window.innerWidth,
      h = c.height = window.innerHeight,
      ctx = c.getContext( '2d' ),

      tick = 0,

      particles = [],

      maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  ctx.font = '12px monospace';

  var rand = Math.random() * (0.06 - 0.01);
  var rand2 = Math.random() * (10 - 6);

  function getRandomColorr() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++ ) {
          color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
  }



  function Particle(){

    this.reset();
  }
  Particle.prototype.reset = function(){

    this.radian = Math.random() * Math.PI * 2;
    this.radius = 0;
    this.angSpeed = rand;
    this.incSpeed = rand2;

    this.x = this.y = 0;
  }
  Particle.prototype.step = function(){

    var prevX = this.x,
        prevY = this.y;

    this.radian += this.angSpeed;
    this.radius += this.incSpeed;

    this.x = this.radius * Math.cos( this.radian );
    this.y = this.radius * Math.sin( this.radian );

    var dx = this.x - prevX,
        dy = this.y - prevY,
        len = Math.sqrt( dx*dx + dy*dy );

    for( var i = 0; i <= len; i += 10 ){

      var y = prevY + dy * i / len,
          x = prevX + dx * i / len;

      var posX = ( x / 10 |0 ) * 10,
          posY = ( y / 10 |0 ) * 10;

  //colors
      var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
      var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

      var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
      var redscale = redscales[Math.floor(Math.random() * redscales.length)];

      var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
      var color420 = colors420[Math.floor(Math.random() * colors420.length)];

      var july4 = ["#ff0000", "#ffffff", "#0048ff"]
      var july4th = july4[Math.floor(Math.random() * july4.length)];
  //colors


  //texts
      var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
      var maze = mazee[Math.floor(Math.random() * mazee.length)];

      var binaryar = ["0", "1"]
      var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

      var devil = ["6", "6", "6"]
      var evil = devil[Math.floor(Math.random() * devil.length)];

      var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
      var quad = quads[Math.floor(Math.random() * quads.length)];

      var excla = ["!", "?"]
      var excl = excla[Math.floor(Math.random() * excla.length)];

      var shades = ["\u2591", "\u2592", "\u2593"]
      var shade = shades[Math.floor(Math.random() * shades.length)];

      var hearts = ["\u2764", "\u2661", "\u2665"]
      var heart = hearts[Math.floor(Math.random() * hearts.length)];

      var randomtexts = [shade, excl, heart, maze, evil, quad]
      var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
  //texts


  // BG Color
      ctx.fillStyle = '#000000';
  // BG Color

      ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

  // change these      vvv
      ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
      ctx.fillText( evil, w/2 + posX, h/2 + posY );
  // change these    ^^^
    }

    if( this.radius >= maxRadius )
      this.reset();
  }

  function anim(){

    window.requestAnimationFrame( anim );

    ++tick;

    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect( 0, 0, w, h );

    if( particles.length < 100 && Math.random() < .3 )
      particles.push( new Particle );

    particles.map( function( particle ){ particle.step(); } );

  }
  ctx.fillStyle = '#00000';
  ctx.fillRect( 0, 0, w, h );
  anim();

  window.addEventListener( 'resize', function(){

    w = c.width = window.innerWidth;
    h = c.height = window.innerHeight;
    ctx.font = '12px monospace';

    ctx.fillStyle = '#00000';
    ctx.fillRect( 0, 0, w, h );

    maxRadius = Math.sqrt( w*w/4 + h*h/4 );

  })
}

function startRedBinary() {


                        var w = c.width = window.innerWidth,
                            h = c.height = window.innerHeight,
                            ctx = c.getContext( '2d' ),

                            tick = 0,

                            particles = [],

                            maxRadius = Math.sqrt( w*w/4 + h*h/4 );

                        ctx.font = '12px monospace';

                        var rand = Math.random() * (0.06 - 0.01);
                        var rand2 = Math.random() * (10 - 6);

                        function getRandomColorr() {
                            var letters = '0123456789ABCDEF';
                            var color = '#';
                            for (var i = 0; i < 6; i++ ) {
                                color += letters[Math.floor(Math.random() * 16)];
                            }
                            return color;
                        }



                        function Particle(){

                          this.reset();
                        }
                        Particle.prototype.reset = function(){

                          this.radian = Math.random() * Math.PI * 2;
                          this.radius = 0;
                          this.angSpeed = rand;
                          this.incSpeed = rand2;

                          this.x = this.y = 0;
                        }
                        Particle.prototype.step = function(){

                          var prevX = this.x,
                              prevY = this.y;

                          this.radian += this.angSpeed;
                          this.radius += this.incSpeed;

                          this.x = this.radius * Math.cos( this.radian );
                          this.y = this.radius * Math.sin( this.radian );

                          var dx = this.x - prevX,
                              dy = this.y - prevY,
                              len = Math.sqrt( dx*dx + dy*dy );

                          for( var i = 0; i <= len; i += 10 ){

                            var y = prevY + dy * i / len,
                                x = prevX + dx * i / len;

                            var posX = ( x / 10 |0 ) * 10,
                                posY = ( y / 10 |0 ) * 10;

                        //colors
                            var greyscales = ["#ffffff", "#efefef", "#e0e0e0", "#c6c6c6", "#a3a3a3", "#9b9b9b", "#828282", "#686868", "#545454", "#3a3a3a"]
                            var greyscale = greyscales[Math.floor(Math.random() * greyscales.length)];

                            var redscales = ["#FF0000", "#F90000", "#EF0000", "#DB0000", "#CE0000", "#CC0000", "#C10000", "#B20000", "#A30000", "#960000", "#820000", "#720000", "#6D0000", "#630000", "#5B0000", "#4C0000", "#AA0000", "#D80202", "#9B0000"]
                            var redscale = redscales[Math.floor(Math.random() * redscales.length)];

                            var colors420 = ["#fce700", "#fc0000", "#5cfc00"]
                            var color420 = colors420[Math.floor(Math.random() * colors420.length)];

                            var july4 = ["#ff0000", "#ffffff", "#0048ff"]
                            var july4th = july4[Math.floor(Math.random() * july4.length)];
                        //colors


                        //texts
                            var mazee = ["\u2594", "\u2595", "\u258F", "\u2581"]
                            var maze = mazee[Math.floor(Math.random() * mazee.length)];

                            var binaryar = ["0", "1"]
                            var binary = binaryar[Math.floor(Math.random() * binaryar.length)];

                            var devil = ["6", "6", "6"]
                            var evil = devil[Math.floor(Math.random() * devil.length)];

                            var quads = ["\u2596", "\u2597", "\u2598", "\u2599", "\u259A", "\u259B", "\u259C", "\u259D", "\u259E", "\u259F"]
                            var quad = quads[Math.floor(Math.random() * quads.length)];

                            var excla = ["!", "?"]
                            var excl = excla[Math.floor(Math.random() * excla.length)];

                            var shades = ["\u2591", "\u2592", "\u2593"]
                            var shade = shades[Math.floor(Math.random() * shades.length)];

                            var hearts = ["\u2764", "\u2661", "\u2665"]
                            var heart = hearts[Math.floor(Math.random() * hearts.length)];

                            var randomtexts = [shade, excl, heart, maze, evil, quad]
                            var randomtext = randomtexts[Math.floor(Math.random() * randomtexts.length)];
                        //texts


                        // BG Color
                        		ctx.fillStyle = '#000000';
                        // BG Color

                        		ctx.fillRect( w/2 + posX, h / 2 + posY - 9, 10, 10 );

                        // change these      vvv
                            ctx.fillStyle = 'hsl(hue,80%,50%)'.replace( 'hue', posX / w * 240 + posY / h * 240 + tick );
                            ctx.fillText( binary, w/2 + posX, h/2 + posY );
                        // change these    ^^^
                          }

                          if( this.radius >= maxRadius )
                            this.reset();
                        }

                        function anim(){

                          window.requestAnimationFrame( anim );

                          ++tick;

                          ctx.fillStyle = 'rgba(0,0,0,0)';
                          ctx.fillRect( 0, 0, w, h );

                          if( particles.length < 100 && Math.random() < .3 )
                            particles.push( new Particle );

                          particles.map( function( particle ){ particle.step(); } );

                        }
                        ctx.fillStyle = '#00000';
                        ctx.fillRect( 0, 0, w, h );
                        anim();

                        window.addEventListener( 'resize', function(){

                          w = c.width = window.innerWidth;
                          h = c.height = window.innerHeight;
                          ctx.font = '12px monospace';

                        	ctx.fillStyle = '#00000';
                        	ctx.fillRect( 0, 0, w, h );

                          maxRadius = Math.sqrt( w*w/4 + h*h/4 );

                        })





}
//thx atlas <3
