# g00n
Ready to make some money? See if you can figure out where the credit card dump is being hosted.

Flag: `dam{d0_th3_criMe_d0_th3_t1m3}`
Hint 1: `Yes, people still use IRC.`
Hint 2: `File carving is a useful skill.`

# CTFd Files/Connection Info
Give them `capture1.pcap`

# Hosting Information
Go program running in Docker container acts as a simple webserver. Dockerfile uses port `8196`. Needs `g00n.damctf.xyz` as a DNS A record pointing to the docker host.