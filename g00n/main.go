package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

/*****
DURING COMPETITION, MAKE SURE THE SECRET.PNG FILE IS NOT BEING HOSTED -- COMMENT OUT THE LINE IN main()
Flag: See flag file.
*****/

func main() {
	// Register our HTTP handlers
	http.HandleFunc("/", henlo)
	http.HandleFunc("/ohai.js", henlojs)
	//http.HandleFunc("/secret.png", secretimg) /*WE ONLY WANT THIS ENABLED DURING PCAP CREATION, DO NOT SERVE DURING CHALLENGE*/
	http.HandleFunc("/ccdump.txt", ccdump)
	http.ListenAndServe(":8196", nil)
}

func henlo(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("ohai.html")
	fmt.Fprintf(w, "%s", body)
}

func henlojs(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("ohai.js")
	fmt.Fprintf(w, "%s", body)
}

/*func secretimg(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("secret.png")
	fmt.Fprintf(w, "%s", body)
}*/

func ccdump(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("flag")
	fmt.Fprintf(w, "%s", body)
}
