from Crypto.PublicKey import RSA
from Crypto.Util.number import *
import os

def solve_crt_with_lin_pad(As, Bs, Cs, Ns):
    Ts = [crt([int(i == j) for j in range(3)], Ns) for i in range(3)]
    P.<x> = PolynomialRing(Zmod(prod(Ns)))
    Gs = [(Ts[i] * ((As[i] * x + Bs[i])**3 - Cs[i])) for i in range(3)]
    g = sum(Gs)
    g = g.monic()

    roots = g.small_roots()
    return long_to_bytes(roots[0])

def solve_stereotyped_message(a, b, c, N, prefix):
    P.<x> = PolynomialRing(Zmod(N))
    g = (a*(prefix + x) + b)^3 - c
    g = g.monic()

    roots = g.small_roots()
    return long_to_bytes(roots[0])

Ns = []
authors = ['stan', 'zander', 'zach', 'lance']
for a in authors:
    with open('pubkey/{}.pub'.format(a), 'r') as f:
        pubkey = RSA.importKey(f.read())
        Ns.append(pubkey.n)

with open('mitm.log') as f:
    log = f.readlines()

exec(log[1])
exec(log[2])
exec(log[3])
m1 = solve_crt_with_lin_pad(As, Bs, Cs, Ns[1:])
print('[*] Decrypted m1: {}'.format(m1))

exec(log[6])
exec(log[7])
exec(log[8])
m2 = solve_crt_with_lin_pad(As, Bs, Cs, Ns[1:])
print('[*] Decrypted m2: {}'.format(m2))

exec(log[11])
exec(log[12])
exec(log[13])
m3 = solve_crt_with_lin_pad(As, Bs, Cs, Ns[1:])
print('[*] Decrypted m3: {}'.format(m3))

prefix_str = os.path.commonprefix([m1, m2, m3])
print('[*] Found common prefix: {}'.format(prefix_str))
prefix_str += '\x00'*(100-len(prefix_str))
prefix = bytes_to_long(prefix_str)

exec(log[16])
exec(log[17])
exec(log[18])
m4 = solve_stereotyped_message(a, b, c, Ns[0], prefix)
print('[*] Decrypted m4: {}'.format(prefix_str + m4))

exec(log[21])
exec(log[22])
exec(log[23])
m5 = solve_stereotyped_message(a, b, c, Ns[0], prefix)
print('[*] Decrypted m5: {}'.format(prefix_str + m5))

exec(log[26])
exec(log[27])
exec(log[28])
m6 = solve_stereotyped_message(a, b, c, Ns[0], prefix)
print('[*] Decrypted m6: {}'.format(prefix_str + m6))

flag = ''.join([m.split('|')[1].strip(' ') for m in [m4, m5, m6]])
print(' [*] Flag: {}'.format(flag))
