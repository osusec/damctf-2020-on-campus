from Crypto.PublicKey import RSA
from Crypto.Util.number import *
from random import randint
from os import urandom

user = 'stan'
authors = ['stan', 'zander', 'zach', 'lance']

def broadcast(m):
    assert(len(m) <= 100)
    m += urandom(100-len(m))

    As = []
    Bs = []
    Cs = []

    for author in authors:
        if author == user:
            continue

        a, b, c = send(m, author)
        As.append(a)
        Bs.append(b)
        Cs.append(c)

    return As, Bs, Cs

def send(m, author):
    assert(len(m) <= 100)
    m += urandom(100-len(m))

    m = bytes_to_long(m)
    with open(f'pubkey/{author}.pub', 'r') as f:
        pubkey = RSA.importKey(f.read())

        e = pubkey.e
        n = pubkey.n

        # improve security w/ linear padding
        a = randint(1, n-1)
        b = randint(1, n-1)
        m = a*m + b

        c = pow(m, e, n)

    return a, b, c
