# Leaker

The other CTF authors created a new chat without me because
they were afraid I would leak more flags. Luckily, I managed
to both recover the code they use to encrypt their messages
and MITM Stan's communications. Can you help me figure out 
what they are saying?

Flag: `dam{w1LLL_s3LLL_fLLL4Gz_4_c0pper}`

Hint: `Twenty Years of CTF Problems on the RSA Cryptosystem`

## CTFd Files/Connection Info

Only give leaker.zip.
