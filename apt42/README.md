# APT42

You've come across some malware as you were pursuing the latest and greatest threat group, APT42. RUMINT has it they enjoy flag hording...

_Note: This is not real malware, and will not harm your computer at all_

Flag: `dam{m41w4r3_1s_n0t_c001_br0}`

Hint 1: `Wireshark is your friend`

## CTFd Files/Connection Info

Give them `wackbot`

## Hosting Information

Python program running in Docker container is the C2 server. Dockerfile uses port 8443. Needs `apt42.damctf.xyz` as a DNS A record pointing to the docker host