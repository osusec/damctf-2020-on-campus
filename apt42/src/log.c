#include "log.h"

bool debug = false;

void log_info(const char *msg) {
    if (!debug) return;

    puts(msg);
}