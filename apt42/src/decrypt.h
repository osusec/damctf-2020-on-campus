#ifndef __DECRYPT_H
#define __DECRYPT_H

#include <stdlib.h>
#include <string.h>

#include <openssl/rc4.h>

#define MAX_STR_LEN 100

void init_str();
const char *get_str(int);
void fin_str();

#endif