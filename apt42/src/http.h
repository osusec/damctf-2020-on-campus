#ifndef __HTTP_H
#define __HTTP_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "decrypt.h"
#include "log.h"

#define MSG_SIZE 1024
#define RESP_SIZE 2048
#define EMPTY_STR ""

extern char *user_agent;
extern char *host;
extern const int port;

char *request(const char *, const char *, const char *, const char *);
char *get_body(const char *);
void add_header(char **, const char *, const char *);
void add_body(char **, const char *);

#endif