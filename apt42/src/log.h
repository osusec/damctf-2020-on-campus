#ifndef __LOG_H
#define __LOG_H

#include <stdbool.h>
#include <stdio.h>

extern bool debug;

void log_info(const char *);

#endif