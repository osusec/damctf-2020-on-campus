#include "http.h"

char *request(const char *path, const char *method, const char *auth_code, const char *body) {
    char *user_agent = "WACKBOT/1.0";
    char *host = "apt42.damctf.xyz";
    const int port = 8443;

    char tmp[MSG_SIZE] = {0};
    char *req = malloc(MSG_SIZE);
    bzero(req, MSG_SIZE);
    char *headers = "%s %s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n";
    // char *headers = "%s %s HTTP/1.0\r\n\r\n";
    char *resp;
    int fd, bytes, counter, total;
    struct hostent *dns;
    struct sockaddr_in serv_addr;

    snprintf(req, MSG_SIZE, headers, method, path, host, user_agent);
    // snprintf(req, MSG_SIZE, headers, method, path);

    // printf("base req: %s", req);
    // puts("");

    if (auth_code) add_header(&req, "X-letmein", auth_code);
    if (body) {
        add_header(&req, "Content-Type", "text/plain");

        bzero(tmp, MSG_SIZE);
        snprintf(tmp, MSG_SIZE, "%ld", strlen(body));
        add_header(&req, "Content-Length", tmp);
    }

    add_body(&req, body);

    if (!(fd = socket(AF_INET, SOCK_STREAM, 0))) {
        log_info("failed to open socket");
        return NULL;
    }

    if (!(dns = gethostbyname(host))) {
        log_info("failed to dns");
        return NULL;
    }

    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    memcpy(&serv_addr.sin_addr.s_addr,dns->h_addr,dns->h_length);

    if (connect(fd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
        log_info("failed to connect");
        return NULL;
    }

    log_info("connected");

    total = strlen(req);
    counter = 0;
    do {
        bytes = write(fd, req+counter, total-counter);
        if (bytes < 0) {
            log_info("failed to write");
            return NULL;
        }
        if (bytes == 0) break;
        counter += bytes;
    } while (counter < total);

    resp = malloc(RESP_SIZE);
    bzero(resp, RESP_SIZE);
    total = RESP_SIZE-1;
    counter = 0;
    do {
        bytes = read(fd, resp+counter, total-counter);
        if (bytes < 0) {
            log_info("failed to read");
            return NULL;
        }
        if (bytes == 0) break;
        counter += bytes;
    } while (counter < total);

    close(fd);
    free(req);

    return resp;
}

char *get_body(const char *resp) {
    return strstr(resp, "\r\n\r\n")+4;
}

void add_header(char **request, const char *name, const char *value) {
    char *tmp = malloc(strlen(*request));
    strcpy(tmp, *request);

    // printf("request: %s", *request);
    // puts("");
    // printf("tmp: %s", tmp);
    // puts("");

    sprintf(*request, "%s%s: %s\r\n", tmp, name, value);

    // printf("new request: %s", *request);

    free(tmp);
    // puts("");
}

void add_body(char **request, const char *body) {
    char *tmp = malloc(strlen(*request));
    strcpy(tmp, *request);

    if (body != NULL) {
        sprintf(*request, "%s\r\n%s", tmp, body);
    } else {
        sprintf(*request, "%s\r\n", tmp);
    }

    free(tmp);
}