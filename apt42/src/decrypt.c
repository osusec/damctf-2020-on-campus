#include "decrypt.h"

const int num_strs = 3;//NUMSTRS
char **strtable = NULL;

const char encstrs[][MAX_STR_LEN] = {"REPLACE_ENC"};

char *password = "REPLACE_PASSWORD";

void init_str() {
    strtable = malloc(num_strs * sizeof(char *));
    for (int i = 0; i < num_strs; i++) {
        strtable[i] = malloc(MAX_STR_LEN);
        bzero(strtable[i], MAX_STR_LEN);
    }
}

const char *get_str(int id) {
    RC4_KEY key;

    if (strlen(strtable[id]) > 0) return strtable[id];

    RC4_set_key(&key, strlen(password), (unsigned char *)password);

    RC4(&key, strlen(encstrs[id]), (unsigned char *)encstrs[id], (unsigned char *)strtable[id]);

    return strtable[id];
}

void fin_str() {
    for (int i = 0; i < num_strs; i++) {
        free(strtable[i]);
    }
    free(strtable);
}