#!/usr/bin/env python3

from arc4 import ARC4
from http import server
import random
import sched
import string
import urllib

class Bot():
    id = None
    auth_code = None

    def __init__(self, id):
        self.id = id
        self.auth_code = self._generate_auth_code() if self._is_valid_id() else "lolnope"

    def _is_valid_id(self):
        """Make sure bot has valid ID"""
        # valid IDs are 20 lowercase alpha
        if len(self.id) != 20:
            return False

        for c in self.id:
            if c not in string.ascii_lowercase:
                return False
        
        return True

    def _generate_auth_code(self, l=40):
        return ''.join(random.choice(string.ascii_letters+string.digits) for x in range(l))

    def get_encrypted_auth_code(self):
        rc4 = ARC4(self.id)
        return rc4.encrypt(self.auth_code)

class WackbotServer(server.SimpleHTTPRequestHandler):
    active_bots = []

    def __init__(self, args, directory, kwargs):
        super().__init__(args, directory, kwargs)

    def _register_bot(self, id):
        """Register a new bot, auto deactivate after 10 seconds"""
        b = Bot(id)
        self.active_bots.append(b)

    def _deactivate_bot(self, b):
        """Deactivate bot by removing it from the active_bot list"""
        print(f"deactivating {b.id}")
        try:
            self.active_bots.remove(b)
        except:
            pass

    def _get_bot(self, id = None, auth = None):
        """Get the Bot object for the given ID, or None"""
        if id is not None:
            return next((b for b in self.active_bots if b.id == id), None)
        if auth is not None:
            if auth == "lolnope": # this is the auth code if the bot has an invalid ID
                return None
            return next((b for b in self.active_bots if b.auth_code == auth), None)
        
        raise ValueError("need to specify id or auth")

    def _is_valid(self):
        """Check if the request was valid for WACKBOT"""
        return self.headers["User-Agent"] == "WACKBOT/1.0"

    def _is_authenticated(self):
        """Check if the bot provided the proper authentication"""
        try:
            auth = self.headers["X-letmein"]
            auth_bot = self._get_bot(auth=auth)
            return auth_bot is not None and auth_bot == self._get_bot(id=auth_bot.id)
        except:
            return False

    def _motd(self):
        """Handle /motd, return the MOTD"""

        messages = [
            "CTF is hard, let's go watch a movie",
            "An apple a day keeps Bill Gates away",
            "Four score and seven years ago we taught sand how to think. Big mistake",
            "You teach best what you most need to learn",
            "You're currently going through a difficult transition period called \"Life.\""
        ]

        if not self._is_authenticated():
            self.send_403()
            return

        self.send_200(random.choice(messages))
    
    def _exit(self):
        """Handle /exit, deactivate the bot"""
        if not self._is_authenticated():
            self.send_403()
            return

        self._deactivate_bot(self._get_bot(auth=self.headers["X-letmein"]))
        self.send_200()

    def _flagrepo(self):
        """Handle /flagrepo, send the flag"""
        if not self._is_authenticated():
            self.send_403()
            return

        try:
            with open("flag", "r") as f:
                flag = f.read()
        except:
            flag = "dam{test}"

        self.send_200(flag)

    def _register(self):
        """Handle /register, register the bot and send back encrypted auth code"""
        # i guarentee there is a better way to do this
        qs = urllib.parse.parse_qs(self.path.split("?")[1])
        id = qs["id"][0]

        self._register_bot(id)
        self.send_200(self._get_bot(id).get_encrypted_auth_code())

    def send_404(self):
        self.send_response(404, "This is not the page you are looking for...")
        self.end_headers()

    def send_403(self):
        self.send_response(403, "Not authorized")
        self.end_headers()

    def send_200(self, body=None):
        self.send_response(200, "OK")
        self.end_headers()
        if body is not None:
            if type(body) is str:
                body = body.encode()
            self.wfile.write(body)

    def do_GET(self):
        """Handle GET requests to the server"""

        # make sure valid request
        if not self._is_valid():
            self.send_404()

        path = urllib.parse.unquote(self.path).split("?")[0]

        # handle different paths
        if path == "/motd":
            self._motd()
        elif path == "/exit":
            self._exit()
        elif path == "/flagrepo":
            self._flagrepo()
        else:
            self.send_404()

    def do_POST(self):
        """Handle POST requests to the server"""

        # make sure valid request
        if not self._is_valid():
            self.send_404()

        path = urllib.parse.unquote(self.path).split("?")[0]

        if path == "/register":
            self._register()
        else:
            self.send_404()

httpd = server.HTTPServer(('0.0.0.0', 8443), WackbotServer)
httpd.serve_forever()