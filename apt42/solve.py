#!/usr/bin/env python3

import arc4
import random
import requests
import string

headers = {
    "User-Agent": "WACKBOT/1.0"
}

id = ''.join(random.choice(string.ascii_lowercase) for x in range(20))

enc_auth_code = requests.post(f"http://apt42.damctf.xyz:8443/register?id={id}", headers=headers).content

rc4 = arc4.ARC4(id)
auth_code = rc4.encrypt(enc_auth_code)

print(f"auth code: {auth_code.decode()}")

headers["X-letmein"] = auth_code

flag = requests.get(f"http://apt42.damctf.xyz:8443/flagrepo", headers=headers).content

print(f"flag: {flag.decode().strip()}")