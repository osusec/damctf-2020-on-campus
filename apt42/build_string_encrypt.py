#!/usr/bin/env python3

from arc4 import ARC4
import binascii
import glob
import os
import random
import shutil
import string
import sys

KEY_LEN = 32

strings = []
key = ""

def generate_key():
    return "".join(random.choice(string.ascii_letters) for i in range(KEY_LEN))

def encrypt_string(s):
    rc4 = ARC4(key)
    return rc4.encrypt(s)

def insert_into(s, i, n):
    """insert n into s at i"""
    return s[:i] + n + s[i:]

def fix_strings():
    # if something is \xcfa, instead of 0xcf and then ascii 'a', it is 0xfa. this is stupid
    # fix the strings so it will produce 0xcf and 'a'
    # "\xcfa" -> "\xcf""a"
    # there's also another problem where a " could naturally occur in the ciphertext
    # if there's one, escape it
    # "\x83a"bc" -> "\x83a\"bc"
    for i, s in enumerate(strings):
        new = repr(s)[2:-1]
        counter = 0
        while counter < len(new):
            if new[counter] == '"':
                if counter == 0:
                    new = '\\' + new
                else:
                    new = insert_into(new, counter, '\\')
                counter += 2
            elif counter < len(new)-1 and (new[counter] == "\\" and new[counter+1] == "x"):
                # we need the " check to run on the whole string but not \xhh
                counter += 4
                new = insert_into(new, counter, '""')
                counter += 2
            else:
                counter += 1

        strings[i] = new 

def encrypt_file(code):
    encrypted = []
    for c in code:
        # don't encrypt includes
        if c[0] == "#":
            encrypted.append(c)
            continue

        if "\"" in c:
            c = c.replace("\\r", "\r")
            c = c.replace("\\n", "\n")

        while "\"" in c:
            # line has strings
            str_id = len(strings)
            split = c.split("\"")
            plain_str = split[1]
            enc_str = encrypt_string(plain_str)
            if enc_str in strings:
                str_id = strings.index(enc_str)
            else:
                strings.append(enc_str)
            c = split[0] + f"get_str({str_id})" + '"'.join(split[2:])

        encrypted.append(c)

    return "".join(encrypted)

def generate_decrypt():
    with open("src/decrypt.c", "r") as f:
        base = f.read()

    # build array of encrypted strings
    fix_strings()
    str_arr = ",".join("\"{}\"".format(str(s)[2:-1] if 'b\'' in str(s)[:2] else str(s)) for s in strings)

    # make the stuff
    base = base.replace("3;//NUMSTRS", (str(len(strings))+";"))
    base = base.replace("\"REPLACE_ENC\"", str_arr)
    base = base.replace("REPLACE_PASSWORD", key)

    return base

def main(argv):
    global key
    if len(argv) == 2:
        key = argv[1]
    else:
        key = generate_key()

    files = glob.glob("src/*.c")

    if os.path.exists("src_compile/"):
        shutil.rmtree("src_compile")
    os.mkdir("src_compile")

    for inf in files:
        # don't encrypt our decryption stub file
        if "decrypt" in inf:
            continue

        with open(inf, "r") as f:
            content = f.readlines()

        out = encrypt_file(content)
        with open(f"src_compile/{inf.split('/')[1]}", "w") as f:
            f.write(out)

    # generate decrypt
    with open("src_compile/decrypt.c", "w") as f:
        f.write(generate_decrypt())

    # copy over header files
    files = glob.glob("src/*.h")
    for f in files:
        shutil.copyfile(f, f"src_compile/{f.split('/')[1]}")

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))