# Matrix

If you look close enough, you can see inside the matrix.

Flag: `dam{m@ny_secrets_few_b1ts}`

Hint `1`: Steganography is pretty cool

## Stuff to fill in
* Makefile
  * Set `CHALLENGE_NAME`
* src/
  * Put your code in
  * If you use more than just `pwn.c` update the Makefile

## CTFd Files/Connection Info

URL or `nc` info

Just give away `lattice.png`

## Hosting Information

What version of Ubuntu/libc (tcache is present on 17.10+)?

Just pass along the image
