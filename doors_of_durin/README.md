# Doors of Durin

Validation is always best done on the client-side

Flag: `dam{$peak_friend_@nd_ENTER}`

Hint `n`: hint

## Stuff to fill in
* Makefile
  * Set `CHALLENGE_NAME`
* src/
  * Put your code in
  * If you use more than just `pwn.c` update the Makefile

## CTFd Files/Connection Info

server runs on port 8080

no files to distribute
## Hosting Information

What version of Ubuntu/libc (tcache is present on 17.10+)?

Any notes about how to host the challenge or special considerations. If the challenge needs multiple containers, what type of network comms b/t them?
