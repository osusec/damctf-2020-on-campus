The challengers will be given the original flag file and
.viminfo that keeps track of the commands that user has run on the file.

# Command Line History (newest to oldest):
:wq
|2,0,1582019920,,"wq"
:%s/G//g
|2,0,1582019916,,"%s/G//g"
:%s/S/5/g
|2,0,1582019912,,"%s/S/5/g"
:%s/3/E/g
|2,0,1582019908,,"%s/3/E/g"
:%s/9/G/g
|2,0,1582019904,,"%s/9/G/g"
:%s/e/3/g
|2,0,1582019898,,"%s/e/3/g"
:%s/1/I/g
|2,0,1582019893,,"%s/1/I/g"
:%s/g/G/g
|2,0,1582019889,,"%s/g/G/g"
:%s/5/S/g
|2,0,1582019885,,"%s/5/S/g"
:%s/I/i/g
|2,0,1582019881,,"%s/I/i/g"
:%s/s/S/g
|2,0,1582019876,,"%s/s/S/g"
:%s/i/1/g
|2,0,1582019872,,"%s/i/1/g"


By repeating these commands in reverse (wq at the end),
one can get the flag.

