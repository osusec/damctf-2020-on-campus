# Shamir Secret Sharing

There is a dam secret in the house. Find enough shares, and reconstruct the secret!

Flag: `dam{Shhhhharing_iS_cAR1ing}`

Hint `1`: See if you can modify the given sage script to reconstruct the secret once you have enough shares.

Hint `2`: Check out sections 3.3 and 3.4 of Mike Rosulek's awesome [cryptography book](https://web.engr.oregonstate.edu/~rosulekm/crypto/chap3.pdf). You can tell he is an OSU professor because he has "osu" in his name.

## CTFd Files/Connection Info

Provide the (partial) source code `shamir.sage`, and its redacted output `shares.txt`.

## Hosting Information

None.
