#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <stdint.h>

char a[4096] = {0};
char name[50] = {0};
char b[4096] = {0};

int protection_num = 0xc35a;

int buffer_protection(void* addr, size_t len, int prot) {
    printf("Well %s", name);
    puts("Here at DAMCTF, we roll our own security protections.");
    printf("Introducing NX v%d!\n", protection_num);
    puts("Who needs the built-in NX flag?");

    return mprotect(addr, len, prot);
}

int main(void) {
    puts("What is your name?");
    fgets(name, 50, stdin);

    char* name_aligned = (char *) ((uint64_t) name & ~(4095));
    buffer_protection(name_aligned, 4096, PROT_READ);

    char feedback[20];
    puts("\nFeel free to type some feedback here:");
    fgets(feedback, 200, stdin);
    puts("Just kidding, we don't care.");

    return 0;
}
