# Challenge Name

Personally, I don't trust anything that I didn't create myself.

Flag: `dam{1f_1T_i5_n)T_iNv3Nt3D_h3rE,I_d0Nt_W4nT_1t!!!!}`

Hint `n`: hint

## Stuff to fill in
* Makefile
  * Set `CHALLENGE_NAME`
* src/
  * Put your code in
  * If you use more than just `pwn.c` update the Makefile

## CTFd Files/Connection Info

URL or `nc` info

Files to distribute (binary, libc, etc)

## Hosting Information

What version of Ubuntu/libc (tcache is present on 17.10+)?

Any notes about how to host the challenge or special considerations. If the challenge needs multiple containers, what type of network comms b/t them?
