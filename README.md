# DamCTF 2020 Challenges

This repo contains the challenges for the onsite DamCTF 2020, held at Oregon State University in February 2020.

[View this event on CTFtime](https://ctftime.org/event/1008/)