# letmein
So you wanna get your hands dirty with some packet analysis? Here is a nice introduction to the craft; see if you can find the login page and correct credentials.

Flag: `dam{pCAPs_r_fUn_http_is_BAD}`
Hint 1: `A packet analysis tool like Zeek or Wireshark might help!`

# CTFd Files/Connection Info
Give them `capture1.pcap`

# Hosting Information
Go program running in Docker container acts as a simple webserver. Dockerfile uses port `6969`. Needs `letmein.damctf.xyz` as a DNS A record pointing to the docker host