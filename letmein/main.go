package main

import (
	"crypto/subtle"
	"fmt"
	"io/ioutil"
	"net/http"
)

/*****
Username: xAxJa8C3
Password: Nmq5wKBJ

Flag: See flag file.
*****/

func main() {
	// Register our HTTP handlers
	http.HandleFunc("/", henlo)
	http.HandleFunc("/ohai.js", henlojs)
	http.HandleFunc("/letmein", basicauth(letmein, "xAxJa8C3", "Nmq5wKBJ", "Login plz"))
	http.ListenAndServe(":6969", nil)
}

func henlo(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("ohai.html")
	fmt.Fprintf(w, "%s", body)
}

func henlojs(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("ohai.js")
	fmt.Fprintf(w, "%s", body)
}

func letmein(w http.ResponseWriter, r *http.Request) {
	damflag, _ := ioutil.ReadFile("flag")

	fmt.Fprintf(w, "<font color=\"red\"><h2><I><marquee scrollamount=\"20\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"orange\"><h2><I><marquee scrollamount=\"10\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"yellow\"><h2><I><marquee scrollamount=\"5\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"green\"><h2><I><marquee scrollamount=\"8\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"blue\"><h2><I><marquee scrollamount=\"2\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"purple\"><h2><I><marquee scrollamount=\"4\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"gray\"><h2><I><marquee scrollamount=\"30\">%s</marquee></I></h2></font>", damflag)
	fmt.Fprintf(w, "<font color=\"brown\"><h2><I><marquee scrollamount=\"6\">%s</marquee></I></h2></font>", damflag)
}

func basicauth(handler http.HandlerFunc, username, password, realm string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, ok := r.BasicAuth()
		if !ok || subtle.ConstantTimeCompare([]byte(user), []byte(username)) != 1 || subtle.ConstantTimeCompare([]byte(pass), []byte(password)) != 1 {
			w.Header().Set("WWW-Authenticate", `Basic realm="`+realm+`"`)
			w.WriteHeader(401)
			w.Write([]byte("Sorry fren, plz try again <3\n"))
			return
		}
		handler(w, r)
	}
}
