# Affinity

Affine ciphers are secure. Change my mind.

Flag: `dam{tO_Aff1nItY_aNd_BEyOnD!!!}`

Hint `1`: Try to write a decryption function first
Hint `2`: Might need to write an "inverse modulus"
Hint `3`: How many possible keys are there?

## CTFd Files/Connection Info

No url, just hand over `ciphertext.txt` as well as `challenge.py`

## Hosting Information

Nothing special, just give out the two files.
