#!/usr/bin/env python3
import string

alphabet = string.ascii_letters + string.digits + "!_{}%"


def encrypt(message, key):
    k1, k2 = key
    enc = ""
    for letter in message:
        enc += alphabet[(k1 * alphabet.index(letter) + k2) % len(alphabet)]
    return enc


def decrypt(ciphertext, key):
    #???
    pass
