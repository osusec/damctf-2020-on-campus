#!/usr/bin/env python3
import string

alphabet = string.ascii_letters + string.digits+"!_{}%"


def encrypt(message, key):
    k1, k2 = key
    enc = ""
    for letter in message:
        enc += alphabet[(k1 * alphabet.index(letter) + k2) % len(alphabet)]
    return enc


def decrypt(ciphertext, key):
    k1, k2 = key
    dec = ""
    for letter in ciphertext:
        difference = (alphabet.index(letter) - k2) % len(alphabet)
        inv = inverse_mod(k1, len(alphabet))
        index = difference * inv % len(alphabet)
        dec += alphabet[int(index)]
    return dec


intercepted = "yEgK7rMTuu6eD7{MEtyMRLXreNOOOI"
for key1 in range(1, len(alphabet)):
    for key2 in range(len(alphabet)):
        attempt = decrypt(intercepted, (key1, key2))
        if "dam{" in attempt:
            print("key:", (key1, key2))
            print("Plaintext:", attempt)
