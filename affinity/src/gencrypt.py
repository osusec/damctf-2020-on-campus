#!/usr/bin/env python3

from challenge import encrypt

def get_ctext():
    key = (65, 30)
    flag = "dam{tO_Aff1nItY_aNd_BEyOnD!!!}"
    print(flag)
    enc = encrypt(flag, key)
    print(enc)
    with open("ciphertext.txt", 'w') as f:
        f.write(enc + '\n')

if __name__ == '__main__':
    get_ctext()
