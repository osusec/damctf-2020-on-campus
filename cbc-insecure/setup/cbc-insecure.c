#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>

#include <openssl/aes.h>

#include "data.h"

// list of user information
Users user_list[MAX_USER];


// global data to track login status
int login_index = -1;
int is_logged_in = 0;

void
printmenu(void) {
    printf("####################\n");
    if (is_logged_in) {
        printf("Logged in as %s\n", user_list[login_index].name);
    }
    printf("1. Login\n");
    printf("2. Logout\n");
    printf("3. Check Info\n");
    printf("4. Admin\n");
    printf("5. Exit\n");
    printf("####################\n");
    printf("Please choice one from the menu:\n");
}

void
gets_no_newline(char *dest, size_t size) {
    memset(dest, 0, size);
    fgets(dest, size, stdin);
    size_t len = strlen(dest);
    if (dest[len-1] == '\n') {
        dest[len-1] = '\0';
    }
}

void
login(void) {
    char name[128];
    char pass[128];

    printf("Please type your name:\n");
    gets_no_newline(name, 32);

    // BLOCK admin login
    if (strcmp("admin", name) == 0) {
        printf("You can't login as admin... it is blocked by admin lol!\n");
        return;
    }

    printf("Please type your password:\n");
    gets_no_newline(pass, 32);

    for (int i=0; i<MAX_USER; ++i) {
        // must be valid
        if (user_list[i].is_valid != 1)
            continue;
        // username
        if (strcmp(name, user_list[i].name) != 0)
            continue;
        // password CHECK!
        if (strcmp(pass, user_list[i].password) == 0) {
            printf("Login user: %s\n", user_list[i].name);
            is_logged_in = 1;
            login_index = i;
            break;
        }
    }

    if (is_logged_in) {
        printf("Login was successful!\n");
    }
    else {
        printf("Login was unsuccessful!\n");
    }
}

void
logout(void) {
    if (is_logged_in != 1) {
        printf("You did not login to the system!\n");
        return;
    }

    login_index = -1;
    is_logged_in = 0;
    printf("Logout was successful!\n");
}

void
check_info(void) {
    char buf[128];
    int selection;

    if (is_logged_in != 1) {
        printf("Please login to the system first....\n");
        return;
    }

    printf("We have the following users:\n");
    for (int i=0; i<MAX_USER; ++i) {
        printf("user_list[%d]: %s\n", i, user_list[i].name);
    }

    while (1) {
        printf("Which user do you want to take a look at (0--%d)?\n", MAX_USER-1);
        fgets(buf, 100, stdin);
        selection = strtol(buf, NULL, 10);
        if (selection >= 0 && selection < MAX_USER) {
            break;
        }
        else {
            printf("Please put a correct number between 0 and %d!\n", MAX_USER-1);
        }
    }

    printf("User information:\n");
    printf("Name: %s\n", user_list[selection].name);
    printf("Money: %ld\n", user_list[selection].money);
    printf("Is admin?: %ld\n", user_list[selection].is_admin);
    printf("Is valid?: %ld\n", user_list[selection].is_valid);
}

void
admin(void) {
    if (is_logged_in != 1) {
        printf("Please login to the system first....\n");
        return;
    }

    if (login_index == -1) {
        printf("Please login to the system first....\n");
        return;
    }

    if (user_list[login_index].is_admin) {
        printf("Wow, you are an admin!\n");
        char buf[512];
        FILE *fp = fopen("flag", "rb");
        fread(buf, 1, 512, fp);
        fclose(fp);
        printf("FLAG %s\n", buf);
    }
    else {
        printf("You are not an admin...\n");
    }
}


void
menu(void) {
    int selection = 0;
    int result;
    char buf[0x80];

    while (1) {
        printmenu();
        fgets(buf, 100, stdin);
        selection = strtol(buf, NULL, 10);
        switch (selection) {
            case 1:
                login();
                break;
            case 2:
                logout();
                break;
            case 3:
                check_info();
                break;
            case 4:
                admin();
                break;
            case 5:
                exit(0);
                break;
            default:
                printf("Please choose from 1--5...\n");
                break;
        }
    }
}

void
decrypt_in(char *key, char *iv, char *encrypted_data) {

    AES_KEY dec_key;
    AES_set_decrypt_key(key, 128, &dec_key);
    AES_cbc_encrypt(encrypted_data, (void*)user_list, sizeof(user_list), &dec_key, iv, AES_DECRYPT);

    free(encrypted_data);
}



void
load_data(char* encrypted_data) {
    FILE *fp = fopen("key", "rb");
    if (fp == NULL) {
        printf("Keyfile (key) not found!\n");
        exit(-1);
    }
    char key[16];
    char iv[16];
    memset(iv, 0, sizeof(iv));
    fread(key, 1, 16, fp);
    fclose(fp);

    decrypt_in(key, iv, encrypted_data);

}

void
read_database(void) {
    printf("Please give me your database.enc file.\n");
    printf("In doing this, pleae transmit exactly %ld bytes..\n", sizeof(user_list));

    char *encrypted_data = malloc(sizeof(user_list));
    memset(encrypted_data, 0, sizeof(user_list));
    fread(encrypted_data, 1, sizeof(user_list), stdin);
    load_data(encrypted_data);
}

int
main(int argc, char **argv, char **envp)
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    read_database();
    menu();
    return 0;
}
