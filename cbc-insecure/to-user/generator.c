#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>

#include <openssl/aes.h>

#include "data.h"

#define ADMIN_PASSWORD "SCRAMBLED_PASSWORD_IS_HERE_DO_NOT_TRY_THIS"

Users user_list[MAX_USER];

void
generate_data(void) {
    strcpy(user_list[0].name, "user");
    strcpy(user_list[0].password, "password");
    user_list[0].money = 0;
    user_list[0].is_admin = 0;
    user_list[0].is_valid = 1;

    strcpy(user_list[1].name, "admin");
    strcpy(user_list[1].password, ADMIN_PASSWORD);
    user_list[1].money = 1000000000;
    user_list[1].is_admin = 1;
    user_list[1].is_valid = 1;

    for (int i=2; i<MAX_USER; ++i) {
        memset(user_list[i].name, 0, sizeof(user_list[i].name));
        memset(user_list[i].password, 0, sizeof(user_list[i].password));
        user_list[i].money = user_list[i].is_valid = user_list[i].is_admin = 0;
    }

    printf("Total size: %ld\n", sizeof(user_list));
}

void
encrypt_out(char *key, char *iv, char *data) {
    char *encrypted_data = malloc(sizeof(user_list));
    memset(encrypted_data, 0, sizeof(user_list));

    AES_KEY enc_key;
    AES_set_encrypt_key(key, 128, &enc_key);
    AES_cbc_encrypt(data, encrypted_data, sizeof(user_list), &enc_key, iv, AES_ENCRYPT);

    FILE *fp = fopen("database.enc", "wb");
    fwrite(encrypted_data, 1, sizeof(user_list), fp);
    fclose(fp);

    free(encrypted_data);
}

int
main(int argc, char **argv, char **envp)
{
    // generate user data
    generate_data();

    char KEY[16];
    char IV[16];

    // read the key securely...
    FILE *fp = fopen("/dev/urandom", "rb");
    if (fp == NULL) {
        perror("Opening file");
        exit(-1);
    }
    fread(KEY, 1, 16, fp);
    fclose(fp);

    // write the key to a file... it's not for you!
    fp = fopen("key", "wb");
    fwrite(KEY, 1, 16, fp);
    fclose(fp);

    for (int i=0; i<16; ++i) {
        // I like zero IV. AES will encrypt everything!
        IV[i] = '\0';
    }

    encrypt_out(KEY, IV, (void*)user_list);

    return 0;
}
