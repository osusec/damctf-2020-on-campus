#ifndef __DATA_H__
#define __DATA_H__

#define MAX_USER (5)

struct Users {
    char name[32];
    char password[40];
    uint64_t is_valid;
    uint64_t money;
    uint64_t is_admin;
}
__attribute__((packed));

typedef struct Users Users;

#endif
