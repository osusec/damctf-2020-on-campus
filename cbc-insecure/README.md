# SETUP
use Dockerfile in setup directory

# Challenge description
username[32]
password[40]
is_valid -- 8 bytes

These are 5 blocks (16 * 5) followed by is_admin and money.

If you alter any single bit in data[80..96], then decryption logic will
decrypt data[0..80] correctly and data[80..end] will be somewhat random.
That is,

username[32] = 'user'
password[40] = 'password'
is_valid = 1;

is_admin = some_random;
money = some_random;

and this is enough to bypass check in admin():

    if (user_list[login_index].is_admin) {      // is_admin == random == Nonzero
        printf("Wow, you are an admin!\n");
        char buf[512];
        FILE *fp = fopen("flag", "rb");
        fread(buf, 1, 512, fp);
        fclose(fp);
        printf("FLAG %s\n", buf);
    }

