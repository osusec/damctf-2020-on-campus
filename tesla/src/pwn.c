#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KEY 0x42

const char *PASSWORD = ";-'.-,57&&72/;&7&'";

void flag() {
    FILE *flag_file;
    char c;

    puts("CORRECT PASSWORD");
    
    flag_file = fopen("flag", "r");

    if (!flag_file) {
        puts("need a flag file");
        return;
    }

    while (true) {
        c = fgetc(flag_file);
        if (c == EOF) return;
        printf("%c", c);
    }
}

bool check_pass(char *pass) {
    int len, i;

    len = strlen(pass);

    if (len != strlen(PASSWORD)) return false;

    for (i = 0; i < len; i++) {
        if ((pass[i] ^ KEY) != PASSWORD[i]) return false;
    }

    return true;
}

int main() {
    char *buf;

    buf = malloc(50);

    puts("TESLA MODEL X");
    puts("PASSWORD PLEASE: ");

    fgets(buf, 50, stdin);
    buf[strcspn(buf, "\n")] = 0;

    if (check_pass(buf)) flag();
    else puts("WRONG PASSWORD");

    free(buf);

    return 0;
}
