#!/usr/bin/env python3

key = 0x42

password = b"yoelonwuddupmydude"

enc_pass = b""

for i, c in enumerate(password):
    # b = (key >> ((i%4)*8)) & 0xff
    # enc_pass += bytes([b ^ c])
    enc_pass += bytes([key ^ c])

print(repr(enc_pass))